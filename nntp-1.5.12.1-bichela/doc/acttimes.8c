.\"
.\" @(#)$Id: acttimes.dst,v 1.2 1994/12/03 21:52:02 sob Exp sob $
.\"
.TH ACTTIMES 8C "16 November 1994"
.UC 4
.SH NAME
acttimes \- Maintain the newsgroup-creation-date file
.SH SYNOPSIS
.I acttimes
[
.B \-d
]
.br
.I acttimes
.B \-k
.SH DESCRIPTION
.I Acttimes
is a program that maintains the active.times file for those systems
whose news software doesn't maintain it for them.
A system that is running C news or INN should already have a valid
.I /usr/lib/news/active.times
file and therefore doesn't need
.I acttimes
to maintain it.
.PP
When executed,
.I acttimes
will create or maintain
.I active.times
by reading the active file,
.IR /usr/lib/news/active ,
and adding any groups it finds that aren't already in the file.
It uses the current time and a fake user-id to create the entry
since it doesn't have access to the article that created the group.
.PP
The
.I active.times
file is a list of each new newsgroup that gets created
on your system, the date it was created, and who asked for it to be created.
This file enables NNTP to respond to the NEWGROUPS command with a list
of groups that are newer than a certain date.
Without the file, the NEWGROUPS command cannot function.
The format is: ``newsgroup date user-id'' where ``newsgroup'' is the name
of the group in question, ``date'' is the newsgroup's
creation time, in seconds since midnight, Jan. 1, 1970, GMT,
and ``user-id'' is the name of the user who created the group, or
``acttimes@local'', if not known.
.PP
.I Acttimes
can be run either as a daemon process, or from cron.
.PP
To run it as a daemon, specify the \-d option and give it the number
of minutes it should wait to wake up (the default is a quick 10 minutes).
For example, ``acttimes -d120'' would start
.I acttimes
and have it wake up every 2 hours.
To kill the daemon, use the command, ``acttimes -k''.
.PP
To run
.I acttimes
from cron, don't specify any options.
A minimum update period is probably once or twice a day, but running it
more often will make the new groups available in a more timely manner.
.SH FILES
LOCKacttimes
.SH AUTHOR
Wayne Davison
.SH SEE ALSO
cron(1),
nntpd(8C)


