/* $Id: conf.h.dist,v 1.8 1994/12/09 02:51:24 sob Exp sob $
 * Configuration information for use by NNTP server and support
 * programs.  Change these as appropriate for your system.
 */

/*
 * Compile time options.
 */
  

#undef	ALONE		/* True if we're running without inetd */

#ifdef ALONE
#undef	FASTFORK        /* True if we don't want to read active file on start*/
#endif

#define LOAD 5	        /* Loadav above which server refuses connections */
/*
 * If you want the article array to be dynamically allocated instead of
 * being fixed at a preset size, define DYNAMIC_ART_ARRAY.
 */
#define DYNAMIC_ART_ARRAY

#undef	BSD_42		/* 4.2 compatability code -- if this is defined, */
			/* DBM probably wants to be defined as well. */

#undef BSD_43		/* Define if you are running on BSD 4.3 */

#undef BSD_44		/* Define if you are running on BSD 4.4 */

#undef CMU_MACH		/* Use CMU's MACH ioctl(FIOCFSPARAM) for dfree(). */

#undef	USG		/* System V support */

#undef	SVR4		/* System V Release 4 support */
#undef STREAMS_TLI	/* Use ioctl(I_STR) to implement ioctl(SIOCGIFCONF) */
#undef SIZE_RETURNED_IN_BUFFER /* if a size integer is first in the buffer */
#undef USE_STREAMS_DEVICE_FOR_IF_CONFIG /* to open /dev/ip rather a socket */

#undef TLI		/* Define this if you want to use TLI instead of */
			/* sockets */

#undef NDBM		/* Use new-style (4.3) ndbm(3x) libraries */

#undef	DBM		/* True if we want to use the old dbm(3x) libraries */
			/* IF YOU DEFINE THIS, change CFLAGS in makefile to */
			/* be -ldbm */

#define	DBZ		/* True if we want to use dbz libraries */
			/* IF YOU DEFINE THIS, change CFLAGS in makefile to */
			/* be /usr/lib/dbz.o and install dbz.h */

#undef	USGHIST		/* Use USG style history file (no DBM) */
			/* IF YOU DO NOT DEFINE NDBM or DBM, this is DEFAULT!*/

#define CNEWS		/* define this if you are running C News */
#define CNEWS_CLEARTEXT	/* define this if you have a modern C News that handles
				input files ending in ".t" as cleartext */
#define BATCHED_INPUT	/* define if you want to support C News style 
				batched input (not supported by B-NEWS)  */
#undef MSGID		/* define this if you want to run msgidd to keep
				track of recent msgids via daemon */

/* Vendor specific implementations */
#undef LAI_TCP		/* Lachman Streams TCP/IP support (Xenix) */
#undef EXCELAN		/* Excelan EXOS 205 support */

/* Things that vary in Unix variations */
#undef U_LONG		/* Define this if your <sys/types.h> is missing */
			/* typedefs for u_long */
#define VOIDSIG		/* Newfangled signal() returns void, old returns int */

#define MMAP		/* if your OS supports mmap() */

#define DIRENT		/* If you have <dirent.h> define this */

/*
 * If you DON'T have vfork, make this "#define vfork fork"
 * vfork will speed up article transfer nntpds by about 2.5 times.
 */

/* #define	vfork fork */

/* Controlling disk and inode consumption */
#define MINFREE 4000	/* NNTP will not allow an XFER if there is less */
			/* than this much diskspace (in blocks or kbytes) */
#define POSTBUFFER 1000 /* NNTP will allow local posting until */
			/* MINFREE-POSTBUFFER blocks or kbytes are left */
/*#define MINFILES  MINFREE/4*/
			/* NNTP will not allow an XFER if there is less */
			/* than this many inodes on the SPOOLDIR filesystem */
#undef SETPROCTITLE	/* if you want status visable via ps */

#undef	IHAVE_DEBUG	/* Copious debugging output from ihave */

/* Things that vary in network implementations */
#define	SUBNET		/* If you have 4.3 subnetting */
#undef	DAMAGED_NETMASK	/* If your subnet mask is not a multiple of */
			/* four bits (e.g., UCSD) */

#undef	NETMASK		/* If you don't have subnet ioctls, define */
			/* this to be a hex constant of your subnet */
			/* mask, e.g., #define NETMASK 0xffffff00 */
			/* Of course, you must define SUBNET above, too. */
#undef	DECNET		/* If you want decnet support */

/*
 * If you want C News batch files created that are not world writable,
 * remove the comments from the UMASK line below. This does not apply
 * if you are running BNEWS. At least, not yet. :-)
 */

#define UMASK 022 

/*
 * If you have the syslog library routine, define SYSLOG to
 * be the syslog facility name under which stats should be
 * logged.  Newer 4.3 systems might choose LOG_NEWS;
 * LOG_LOCAL7 is an acceptable substitute.
 *
 * If you don't have support for syslog, but want a facsimile,
 * define FAKESYSLOG to be the name of a file to which to log stuff,
 * then define SYSLOG and LOG, too.  e.g.,
 *
 *	#define	FAKESYSLOG	"/usr/lib/news/nntplog"
 *
 * If your host supports the BSD fdopen() function and the O_APPEND flag
 * to open(), you should define FAKEAPPEND with FAKESYSLOG so that
 * multiple copies of nntpd don't trash the log with buffered fprintf's.
 *
 * If you don't want any syslog-type activity, #undef SYSLOG.
 * Obviously, this means that you can't define LOG, either.
 */

/*#define FAKESYSLOG	"/usr/lib/news/nntplog"*/
#undef	FAKEAPPEND

#define	SYSLOG	LOG_NEWS

#ifdef SYSLOG		/* Define LOG if you want copious logging info */
#define	 LOG		/* undef it if you don't */
#endif			/* but you can only have LOG if you have SYSLOG */

/*
 * How long you want nntp servers to hang out without receiving
 * commands before they close the connection with an error message.
 *
 * If you don't want any timeout, #undef it, i.e.,
 *
 *	#undef	TIMEOUT
 *
 * TIMEOUT should be at least two hours, which allows users some time
 * away from their terminal (e.g., at lunch) while reading news.
 */

#define	TIMEOUT	(2 * 3600)


/*
 * How long you want nntp servers to wait without receiving data
 * during article transfers.  You CANNOT have XFER_TIMEOUT while
 * running in standalone (ALONE) mode.
 *
 * If you don't want any transfer timeouts, #undef it, as above.
 */

#ifndef ALONE
#   define	XFER_TIMEOUT	(30 * 60)
#endif

/* Various protocol extensions */
#define	XHDR		/* Optional XHDR command.  Defining this will */
			/* speed up '=' command in rn, but will load */
			/* the server more.  If your server is heavily */
			/* loaded already, defining this may be a bad idea */
#define LISTGROUP	/* Optional LISTGROUP command to list article nums */
#define XOVER		/* xover -- Return .overview data */
#undef OVER_XREFS	/* overview files have xref data in them */
#define OVER_XREF_PREFIX /* the xref field (if present) includes xref: */
#define OVER_FMT_FILE	"/usr/lib/news/nntp/overview.fmt"

/* Leave this undefined to indicate that overview files go in the spool
 * directory.  However, if you want a separate hierarchy of overview
 * files, define it here.
 */
/*#define OVERVIEW_DIR "/usr/spool/overviews"	/* base directory */

#define XROVER		/* xrover -- Return reference overview data */

/* Leave this undefined to indicate that ref-overview files go in the
 * spool directory.  However, if you want a separate hierarchy of rover
 * files, define it here.
 */
/*#define ROVER_DIR "/usr/spool/overviews"	/* base directory */

#undef XINDEX		/* Optional support for tin's index files */
#define XINDEX_DIR	"/usr/spool/news/.index"

/* XTHREAD defines:  if XTHREAD is defined, THREAD_DIR controls where the
 * thread files will be read from.
 */
#undef XTHREAD		/* Optional XTHREAD command.  This allows trn to
			 * keep all data on the server. */

/* Leave this undefined to indicate that thread files go in the spool
 * directory.  However, if you want a separate hierarchy of thread files,
 * define it here.
 */
/*#define THREAD_DIR	"/usr/spool/threads"		/* base directory */

/* if LONG_THREAD_NAMES & THREAD_DIR are defined, create threads in one dir */
#undef LONG_THREAD_NAMES		/* not for short-name systems */

/*
 * Person (user name) to post news as.
 */

#define	POSTER		"news"

/*
 * Define DOMAINMATCH if you want to use domain specifications in the 
 * access list instead of just hostnames. See README for more information 
 */
#define DOMAINMATCH

/* Things that relate to authentication and access */
/* Define AUTH to use the proposed NNTP Version 2 authentication protocol. */
#undef	AUTH	

#ifdef AUTH
/* If defined, is directory containing generic authenticators. Since the
 * client gets to choose what programs to be issued, this directory MUST
 * NOT contain anything else but authenticators.
 */
/*#define GENAUTH 	"/usr/lib/news/auth"	*/

/* 
 * the file where the nntpxmit site/userid/passwords are kept
 * think (and PROTECT!) this file like L.sys, i.e., mode 600
 */
# define	PASSFILE	"/etc/nntp.sys"
#endif

/*
 * This file is where access information is stored
 */
#define ACCESS_FILE	"/etc/news/nntp_access"

/*
 * The file where the newgroup creation times are kept.  If you don't
 * have C News or INN, you can run acttimes (see the support directory)
 * to maintain an active.times file.
 */
#define ACTIVE_TIMES_FILE	"/usr/lib/news/active.times"

/*
 * Some commonly used programs and files.
 */

#define	ACTIVE_FILE	"/usr/lib/news/active"
#define DISTRIBUTIONS_FILE	"/usr/lib/news/distributions"
#define SUBSCRIPTIONS_FILE	"/usr/lib/news/subscriptions"
#define NEWSGROUPS_FILE	"/usr/lib/news/newsgroups"
#define HISTORY_FILE	"/usr/lib/news/history"
#define	SPOOLDIR	"/var/spool/news"
#define INEWS		"/usr/bin/inews"
#define RNEWS		"/usr/lib/newsbin/input/rnews"	/* Link to inews? */

/*
 * Support for C News style batching 
 */
#undef  NONEWSRUN		/* define this if you are using the daemon */
				/* version of relaynews */
#ifdef NONEWSRUN
#define TOOBIG 1L		
#define TOOMANY 1
#else
#define TOOBIG 300000L		/* batch > TOOBIG bytes, kick rnews */
#define TOOMANY 1024		/* batch > TOOMANY articles, kick rnews */
#define NEWSRUN			"/usr/lib/news/input/newsrun"
#endif
#define TOOOLD (5*60)		/* batch > TOOOLD seconds old, kick rnews */
#define COPYSIZE 8192		/* bytes to copy at one time */
#define MAXDIGITS 25		/* lg(maxlongint) + epsilon */
#define MAXSTR 1024
#define INDIR			"/var/spool/news/in.coming"
/* You may wish to delete the pathname from the front of BATCH_FILE */
#define BATCH_FILE		"/var/spool/news/in.coming/nntp.XXXXXX"

/************************************************************************/
/* We don't recommend that you make changes in anything after this line */
/************************************************************************/

#ifdef ultrix
/* ultrix is not BSD_44 */
#   ifdef BSD_44
#     undef BSD_44
#   endif
#   ifdef __ultrix
/* Ultrix 4.X is like BSD_43 */
#     ifndef BSD_43
#        define BSD_43
#     endif
/* Ultrix 4.X is not like BSD_42 */
#     ifdef BSD_42
#       undef BSD_42
#     endif
#  else
/* Ultrix 3.X is not like BSD_43 */
#     ifndef BSD_43
#        undef BSD_43
#     endif
/* Ultrix 3.X is like BSD_42 */
#     ifndef BSD_42
#        define BSD_42
#     endif
#  endif /* not __ultrix */
#endif

#ifndef BSD_44
#if defined(__FreeBSD__) || defined(__386BSD__) || defined(bsdi)
#define BSD_44
#endif
#endif

#if defined(__svr4__) || defined(_SYSTYPE_SVR4) || defined(__SVR4)
#ifndef SVR4
#define SVR4
#endif
#ifndef VOIDSIG
#define VOIDSIG
#endif
#ifndef DIRENT
#define DIRENT
#endif
#endif

/* for SunOS 4.X */
#ifdef sun
#   ifdef BSD_44
#     undef BSD_44
#   endif
#   ifndef SVR4
#     ifndef BSD_43
#        define BSD_43
#     endif
#   endif
#endif

#if defined(SVR4) || defined(hpux)
#ifndef USG
#define USG
#endif
#ifdef READ_SUPER
#undef READ_SUPER
#endif
#ifdef BSD_42
#undef BSD_42
#endif
#ifdef BSD_43
#undef BSD_43
#endif
#ifdef BSD_44
#undef BSD_44
#endif
#endif

#ifdef DBZ		/* If you use DBZ, then you need DBM as well. */
#ifndef DBM
#define DBM
#endif /* DBM */
#endif /* DBZ */

#ifdef BSD_42		/* This is a logical, warranted assumption */
#   ifdef NDBM
#       undef NDBM	/* which will probably get me in trouble. */
#   endif
#   ifndef DBM		
#	define DBM	/* Kill it if you have 4.2 *and* ndbm.  */
#   endif
#   ifndef sun		/* not a sun */
#       ifndef ultrix   /* not ultrix */
#           ifndef CMU_MACH /* not CMU's Mach */
#		ifndef NeXT /* not a NeXT */
#                  ifndef READ_SUPER
#	                define READ_SUPER /* read super block for space() */
#		    endif
#               endif
#           endif
#       endif
#   endif
#endif /* BSD_42 */

#ifndef USG
#    ifndef BSD_42
#        ifndef CMU_MACH
#            ifndef BSD_43
#                ifndef BSD_44
#                   define BSD_44
#                endif
#            endif
#        endif
#    endif
#endif

#ifdef BSD_43		/* And now more assumptions! */
#   ifndef sun
#	ifndef __ultrix	/* Ultrix 4.0 or greater */
#	    ifndef mips	/* MIPS RISC/os */
#		ifndef __osf__	/* DEC Alpha OSF1 */
#		    ifndef NeXT /* not a NeXT */
#			ifndef READ_SUPER
#			    define READ_SUPER
#			endif
#		    endif
#		endif
#	    endif
#	endif
#   endif
#endif
#if defined(BSD_43) || defined(BSD_44)
#       ifndef VOIDSIG
#               define VOIDSIG
#       endif
#	ifndef DBZ
#		ifndef DBM
#			ifndef NDBM
#				define NDBM
#			endif
#		endif
#	endif
#endif

#ifdef XTHREAD
# ifdef THREAD_DIR
#   ifdef LONG_THREAD_NAMES
#	undef SUFFIX
#   else
#     ifndef SUFFIX
#	define SUFFIX ".th"
#     endif
#   endif
# else
#   define THREAD_DIR	SPOOLDIR
#   ifndef SUFFIX
#     define SUFFIX	"/.thread"
#   endif
#   undef LONG_THREAD_NAMES
# endif
#endif

#ifdef XOVER
# ifndef OVERVIEW_DIR
#   define OVERVIEW_DIR SPOOLDIR
# endif
#endif

/*
 * System V compatability
 */

#ifdef USG
# define	FCNTL			/* If O_etc is defined in <fcntl.h> */
#define	vfork fork
#ifdef dgux
#define		FTRUNCATE
#else
#ifndef SVR4
# define	NDIR			/* If you  need ndir library support */
#endif
#ifdef hpux
#define		DIRSIZ_MACRO
#endif
#endif

# ifndef index
#  define	index	strchr
# endif
# ifndef rindex
#  define	rindex	strrchr
# endif
# ifndef bcopy
#  define bcopy(s,d,l) memcpy((d),(s),(l))
# endif
# ifndef bcmp
#  define bcmp(s,d,l)  memcmp((s),(d),(l))
# endif
# ifndef bzero
#  define bzero(s,l)   memset((s),0,(l))
# endif
# ifdef U_LONG
   typedef	unsigned long	u_long;
   typedef	unsigned short	u_short;
# endif
# define	IPPORT_NNTP	119
#endif /* USG */


/*
 * Some miscellaneous stuff you probably don't want to change.
 */

#define	MAX_ARTICLES	4096		/* Maximum number of articles/group */
#define READINTVL	60 * 10		/* 10 minutes b/n chking active file */

#ifdef VOIDSIG
#   define SIGRET void
#else
#   define SIGRET int
#endif
