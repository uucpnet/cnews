/*
 *------------------------------------------------------------------
 * $Source: /afs/net.mit.edu/dev/project/techinfodev/src/srv_ti/RCS/nlist.h,v $
 * $Revision: 3.2 $
 * $Date: 93/02/03 11:26:52 $
 * $Author: thorne $
 *------------------------------------------------------------------
 */
/*
 * nlist.h
 */

typedef struct node_list {
	NODE           *n;
	u_short         lev;
} NLIST;

#include <strings.h>




extern void	nlist_fio	(char *file, int op, int msgs);
extern void	nlist_reset	(void);
extern short	nlist_numnode	(void);
extern NODE *	nlist_node	(int relative_id);
extern short	nlist_lev	(int relative_id);
extern void	nlist_add	(NODE *node, u_short depth);
extern void	nlist_add_srch	(NODE *node, u_short depth);
extern void	nlist_build	(int dir, NODE *node, u_short levels);
extern void	nlist_expand	(u_short levels);
extern void     nlist_changed_since(NODE *node, u_short depth);
extern void     nlist_source_srch(NODE *node, u_short depth);

extern NLIST	*nlist_current	();

extern char *   nlist_fmt_line (int relative_id);
extern void     nlist_add_srchflag  (NODE *node, u_short depth);
