# make header file
# pathname variables
# =()<NEWSARTS=@<NEWSARTS>@>()=
NEWSARTS=/var/spool/news
# =()<NEWSOV=@<NEWSOV>@>()=
NEWSOV=/var/spool/news
# =()<NEWSBIN=@<NEWSBIN>@>()=
NEWSBIN=/usr/lib/newsbin
# =()<NEWSCTL=@<NEWSCTL>@>()=
NEWSCTL=/usr/lib/news
# =()<NEWSCONFIG=@<NEWSCONFIG>@>()=
NEWSCONFIG=/usr/lib/news/config

# workaround for System V make bug
SHELL=/bin/sh

# directories where things go
UIBIN=/usr/bin
RBIN=/usr/bin

# compilation stuff
# LIB is for makefile dependencies, LIBS for cc invocations
LIB=../libcnews.a
LIBS=../libcnews.a 
CFLAGS=$(MORECFLAGS) -O2  -I../include
LDFLAGS=$(CFLAGS) 
LINTFLAGS=$(MORELINTFLAGS) -I../include

# directories etc.
CONF=../conf
MAKE=make
TO=$(CONF)/cpto
IN=$(CONF)/cmpto $(CMPOPT)
CF=$(CONF)/checkfile $(CMPOPT)
MKDIR=$(CONF)/mkdirs
MX=chmod +x
UPDATE=$(CONF)/update.sym ../libcnews.a
LIBCMP=$(CONF)/libcmp ../libcnews.a

# misc configuration bits
SPACEFOR=statvfs
DOSPACEFOR=dospacefor
QUEUELEN=svr4
UID=news
GID=news
SIZE=big
DBZ=libdbz
STDIO=sysstdio
SERVER=
REPORTTO=newsmaster
URGENTTO=newsmaster

# things for testing
HERE=. ../conf/rsetup ;

# fake files needed
HFAKE= sys/timeb.h
OFAKE= fgetline.o fsync.o
